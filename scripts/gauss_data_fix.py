# -*- coding: utf-8 -*-
"""
Created on Thu May  1 19:59:25 2014

@author: stuart
"""
import os
import sys
import glob

import h5py
import numpy as np

sys.path.append('./')
from scripts import sacconfig
cfg = sacconfig.SACConfig()

from pysac.io.legacy import VACdata

init = VACdata(os.path.expanduser('~/data/ini/3D_tube_128_128_128.ini'))
X = init.x.copy()

X *= 100 #in cm
zz = X[...,0].T
xx = X[...,1].T
yy = X[...,2].T

new_x = np.zeros([3]+list(zz.shape))
new_x[0] = xx
new_x[1] = yy
new_x[2] = zz

print 'file', X.shape, X[-1,-1,-1,:]

print new_x[:,0,0,0], new_x[:,-1,-1,-1]

domain_left_edge = new_x[:,0,0,0]
domain_right_edge = new_x[:,-1,-1,-1]

#files = glob.glob(os.path.join(cfg.gdf_dir,"*{}_0*.gdf".format(cfg.str_exp_fac)))
#
##broken_keys = ['mag_field_x_pert', 'mag_field_x_bg', 
##               'mag_field_y_pert', 'mag_field_y_bg', 
##               'mag_field_z_pert', 'mag_field_z_bg']
#
#broken_keys = ['internal_energy', 'internal_energy_pert']
#
#for afile in files:
#    print afile
#    f = h5py.File(afile, mode='r+')
#    #f['field_types'][broken_keys[0]].attrs['field_to_cgs'] = 1e4
#    f['x'] = new_x
#    f['simulation_parameters'].attrs['domain_left_edge'] = domain_left_edge
#    f['simulation_parameters'].attrs['domain_right_edge'] = domain_right_edge
#    
#    for key in broken_keys:
#        f['field_types'][key].attrs['field_to_cgs'] = 10
#    f.close()
