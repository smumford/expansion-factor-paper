#!/usr/bin/env python
"""
Created on Mon Oct 21 14:05:06 2013

Parallel conversion from out to gdf
"""
import os

import numpy as np
import h5py

import pysac.io
import pysac.io.legacy
import pysac.io.util
import pysac.io.gdf_writer
from pysac.io.gdf_fields import sac_gdf_output

import sys
sys.path.append('../')
from scripts import sacconfig

try:
    from mpi4py import MPI

    comm = MPI.COMM_WORLD
    rank = MPI.COMM_WORLD.rank

    mpi = True
    mpi = mpi and (comm.size != 1)
except ImportError:
    mpi = False
    rank = 0

cfg = sacconfig.SACConfig()

mpi_config = cfg.mpi_config
output_path = cfg.gdf_dir
if rank == 0: #Prevents race condition where one processes creates the dir
    if not os.path.exists(output_path):
        os.mkdir(output_path)

if mpi:
    input_name = os.path.join(cfg.out_dir,
                        '3D_tube128_%s_%s_%03i.out'%(cfg.get_identifier(),
                                                    cfg.mpi_config, rank))
else:
    input_name = os.path.join(cfg.out_dir,
                        '3D_tube128_%s_%03i.out'%(cfg.get_identifier(), rank))

print '#', rank, '#:', input_name
vfile = pysac.io.legacy.VACfile(input_name)

mu = 1.25663706e-6
#==============================================================================
# Monkey patch varnames and make fields dictionary
#==============================================================================
def get_header_fields(vfile):
    header = vfile.header
    header['varnames'] = cfg.varnames
    indices = range(0,len(header['varnames']))
    w_ = dict(zip(header['varnames'],indices))

    #Convert w
    w = pysac.io.util.mag_convert(vfile.w,w_)
    rhot = w[w_['h']] + w[w_['rhob']]


    fields = sac_gdf_output
    fields['density_pert'].update({'field':w[w_['h']]})
    fields['density_bg'].update({'field':w[w_['rhob']]})
    fields['mag_field_x_pert'].update({'field':w[w_['b2']]})
    fields['mag_field_y_pert'].update({'field':w[w_['b3']]})
    fields['mag_field_z_pert'].update({'field':w[w_['b1']]})
    fields['mag_field_x_bg'].update({'field':w[w_['bg2']]})
    fields['mag_field_y_bg'].update({'field':w[w_['bg3']]})
    fields['mag_field_z_bg'].update({'field':w[w_['bg1']]})
    fields['velocity_x'].update({'field':w[w_['m2']]/rhot})
    fields['velocity_y'].update({'field':w[w_['m3']]/rhot})
    fields['velocity_z'].update({'field':w[w_['m1']]/rhot})
    fields['internal_energy_pert'].update({'field':w[w_['e']]})
    fields['internal_energy_bg'].update({'field':w[w_['eb']]})


    for field in fields.values():
        field['field'] = np.ascontiguousarray(np.rollaxis(field['field'],0,3))
    return header, fields

header, fields = get_header_fields(vfile)
#==============================================================================
# Decide how big the whole array is and what slice of it this processor has
#==============================================================================
nx = header['nx']
#Split sizes
n0 = int(mpi_config[2:4])
n1 = int(mpi_config[4:6])
n2 = int(mpi_config[6:8])

full_nx = [nx[0]*n0, nx[1]*n1, nx[2]*n2]
header['nx'] = full_nx
print rank, nx, full_nx, vfile.x.shape
coords = np.zeros(3)
if rank < n0:
    coords[0] = rank
elif (rank == n0):
    coords[1] = 1
elif rank < (n0 + n1):
    coords[0] = rank - n0
    coords[1] = rank - n1
else:
    coords[2] = rank / (n0 * n1)
    rank2 = rank - (coords[2] * n2)

    if rank2 < n0:
        coords[0] = rank2
    elif rank2 == n0:
        coords[1] = 1
    elif rank2 < (n0 + n0):
        coords[0] = rank2 - n0
        coords[1] = rank2 - n1

s = map(int, coords * nx)
e = map(int, (coords+1) * nx)
arr_slice =  np.s_[s[1]:e[1],s[2]:e[2],s[0]:e[0]]

#==============================================================================
# Reconstruct the whole x array on all the processors
#==============================================================================
x_slice =  np.s_[s[1]:e[1],s[0]:e[0],s[2]:e[2],:]
x = np.zeros(full_nx+[3])
x[x_slice] = vfile.x

zz = x[...,0].T
xx = x[...,1].T
yy = x[...,2].T

new_x = np.zeros([3]+list(zz.shape))
new_x[0] = xx
new_x[1] = yy
new_x[2] = zz

if mpi:
    x_g = comm.gather(new_x, root=0)
    if rank == 0:
        x_0 = np.sum(x_g,axis=0)
    else:
        x_0 = None
    x = comm.bcast(x_0, root=0)

else:
    x = new_x

#==============================================================================
# Save a gdf file
#==============================================================================
for i in range(0,vfile.num_records,1):
    print '#', rank, '#', "read step %i"%i
    vfile.read_timestep(i+1)
    header, fields = get_header_fields(vfile)
    header['nx'] = full_nx

    if mpi:
        f = h5py.File(os.path.join(output_path,
                               cfg.get_identifier()+'_%05i.gdf'%(i+1)),
                  'w', driver='mpio', comm=MPI.COMM_WORLD)
    else:
        f = h5py.File(os.path.join(output_path,
                               cfg.get_identifier()+'_%05i.gdf'%(i+1)),
                  'w')


    domain_left_edge = [x[0][0,0,0]*100, #100 cm in a m
                        x[1][0,0,0]*100, #100 cm in a m
                        x[2][0,0,0]*100] #100 cm in a m
    domain_right_edge = [x[0][-1,-1,-1]*100, #100 cm in a m
                         x[1][-1,-1,-1]*100, #100 cm in a m
                         x[2][-1,-1,-1]*100] #100 cm in a m

    pysac.io.gdf_writer.create_file(f, header, domain_left_edge=domain_left_edge,
                domain_right_edge=domain_right_edge, data_author="Stuart Mumford",
                data_comment="Converted from outfiles in parallel")

    #Write the x array
    f['x'] = x

    if mpi:
        collective = True
    else:
        collective = False

    for field_title,afield in fields.items():
        pysac.io.gdf_writer.write_field(f, afield, field_shape=header['nx'],
                arr_slice=arr_slice, collective=collective, api='low')

    f.close()

print "rank %i finishes"%rank
